import { useEffect, useRef, useState } from "react";

const Ref = () => {
  const [input, setInput] = useState("");

  /** @type {ReturnType<typeof useRef<HTMLInputElement>>} */
  const inputRef = useRef();
  /**
   * {
   *    current: e.target
   * }
   */
  console.log("Rerender!");

//   if (inputRef.current) {
//     inputRef.current.onchange = (e) => {
//       console.log(e.target);
//     };
//   }

//   useEffect(() => {
//     const interval = setInterval(() => {
//       console.log(inputRef.current.value);
//     }, 1000);

//     return () => clearInterval(interval);
//   }, []);

const onInputChange = (e) => setInput(e.target.value);

  return (
    <input
      ref={inputRef}
      type="text"
      placeholder="Username"
        value={input}
        onChange={onInputChange}
    />
  );
};

export default Ref;
