// import { useState } from "react";
import "./App.css";
// import State from "./State";
import Effect from "./Effect";
// import Callback from "./Callback";
// import Ref from "./Ref";
// import Reducer from "./Reducer";

function App() {
  // return <State />;
  return <Effect />;
  // return <Callback />;
  // return <Ref />;
  // return <Reducer />;
}

export default App;
