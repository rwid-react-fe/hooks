import { memo } from "react";

const Count = memo(({ inc, count }) => {
  return (
    <>
      <button onClick={inc}>Add counter</button>
      <div>Count : {count}</div>
    </>
  );
});

export default Count;
// export default Count;
