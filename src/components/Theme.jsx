import { memo } from "react";

const Theme = memo(({ toggleTheme, theme }) => {
  return (
    <>
      <button onClick={toggleTheme}>Toggle theme</button>
      <div>Theme : {theme}</div>
    </>
  );
});

// export default memo(Theme);
export default Theme;
