import { useState } from "react";

const State = () => {
  const [value, setValue] = useState("");

  /** @type {JSX.IntrinsicElements["input"]["onChange"]}  */
  const onInputChange = (evt) => {
    setValue(() => evt.target.value);
  };

  /**
   * 1. Declaration
   * 2. Rules -> !inside of conditional block
   * 3. function state, call() and () => call()
   *
   *
   * memoization
   */

  console.log("Rerender!");

  return (
    <>
      <input value={value} onChange={onInputChange} />
    </>
  );
};

export default State;
