import { useState, useCallback } from "react";
import Count from "./components/Count";
import Theme from "./components/Theme";

const Callback = () => {
  const [count, setCount] = useState(0);
  const [theme, setTheme] = useState("light");

  console.log("Rerender! [Callback.jsx]");

  // without useCallback
  // const inc = () => {
  //   setCount((prevcount) => ++prevcount);
  // };
  // const toggleTheme = () => {
  //   setTheme((prevtheme) => (prevtheme == "light" ? "dark" : "light"));
  // };

  // using useCallback
  const inc = useCallback(() => {
    setCount((prevcount) => ++prevcount);
  }, []);
  const toggleTheme = useCallback(() => {
    setTheme((prevtheme) => (prevtheme == "light" ? "dark" : "light"));
  }, []);

  return (
    <div>
      <Count inc={inc} count={count} />
      {/* <Theme theme={theme} /> */}
      <Theme toggleTheme={toggleTheme} theme={theme} />
    </div>
  );
};

// export default memo(Callback);
export default Callback;
