import { useCallback, useReducer, useState } from "react";
import Count from "./components/Count";
import Theme from "./components/Theme";
import { useWindowSize } from "./hooks/useWindowResize";

const initialState = {
  count: 0,
  theme: "light",
};

// const initialState = undefined;

function reducer(state, action) {
  const { type } = action;

  switch (type) {
    case "inc":
      state = {
        ...state,
        count: state.count + 1,
      };
      return state;

    case "theme":
      //   state.theme = state.theme == "light" ? "dark" : "light";
      state = {
        ...state,
        theme: state.theme == "light" ? "dark" : "light",
      };
      return state;
    default:
      return state;
  }
}

const Reducer = () => {
  //   const [count, setCount] = useState(0);
  //   const [theme, setTheme] = useState("light");

  const [state, dispatch] = useReducer(reducer, initialState);
  //   const size = useWindowSize();

  // without useCallback
  // const inc = () => {
  //   setCount((prevcount) => ++prevcount);
  // };
  // const toggleTheme = () => {
  //   setTheme((prevtheme) => (prevtheme == "light" ? "dark" : "light"));
  // };

  // using useCallback
  //   const inc = useCallback((number) => {
  //     setCount((prevcount) => (prevcount += number));
  //   }, []);
  //   const toggleTheme = useCallback(() => {
  //     setTheme((prevtheme) => (prevtheme == "light" ? "dark" : "light"));
  //   }, []);

  const inc = () => dispatch({ type: "inc" });
  const toggleTheme = () => dispatch({ type: "theme" });

  return (
    <div>
      <Count inc={inc} count={state.count} />
      {/* <Theme theme={theme} /> */}
      <Theme toggleTheme={toggleTheme} theme={state.theme} />
    </div>
  );
};

export default Reducer;
