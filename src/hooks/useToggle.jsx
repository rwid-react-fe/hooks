import { useDebugValue, useState } from "react";

const useToggle = (initialState = false) => {
  const [state, setState] = useState(initialState);

  const toggleState = () => setState((prevstate) => !prevstate);

  useDebugValue(state ? "Active" : "Inactive");

  return [state, toggleState];
};

export default useToggle;
