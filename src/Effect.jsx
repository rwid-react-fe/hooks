import { useState, useEffect } from "react";
import axios from "axios";
import useToggle from "./hooks/useToggle";

const Effect = () => {
  const [char, setChar] = useState("saitama");
  const [data, setData] = useState({});

  const [isLoading, toggleLoading] = useToggle(false);

  /**
   * 1. Declaration
   * 2. Rerender inside outside
   * 3. array deps
   * 4. fetch API -> https://animechan.xyz/api/random/character?name=sai, https://jsonplaceholder.typicode.com/
   */

  const getQuoteByChar =
    /**
     * @param {string} character
     * @param {import("axios").AxiosRequestConfig} options
     */
    async (character, options) => {
      try {
        const response = await axios.get(
          `https://animechan.xyz/api/random/character?name=${character}`,
          options
        );

        setData(response.data);
      } catch (err) {
        console.log(err);
      }

      toggleLoading(false);
    };

  /** @type {JSX.IntrinsicElements["input"]["onChange"]}  */
  const onInputChange = (evt) => {
    setChar(() => evt.target.value);
  };

  useEffect(() => {
    const controller = new AbortController();

    // axios
    //   .get(`https://animechan.xyz/api/random/character?name=${char}`, {
    //     signal: controller.signal,
    //   })
    //   .then((response) => setData(response))
    //   .catch((err) => console.log(err));

    toggleLoading();

    getQuoteByChar(char, {
      signal: controller.signal,
    });

    return () => controller.abort();
  }, [char]);

  return (
    <>
      <div style={{ display: "flex" }}>
        <input value={char} onChange={onInputChange} />
      </div>
      {isLoading ? (
        <p>Loading...</p>
      ) : (
        <div style={{ whiteSpace: "pre", textAlign: "justify" }}>
          {JSON.stringify(data, null, 4)}
        </div>
      )}
    </>
  );
};

export default Effect;
